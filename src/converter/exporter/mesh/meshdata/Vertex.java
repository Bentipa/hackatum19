package converter.exporter.mesh.meshdata;

import converter.util.Position;

public class Vertex {

    private Position pos;

    /**
     * A Vertex is a simple Node with a Position
     * @param pos
     */
    public Vertex(Position pos) {
        this.pos = pos;
    }

    public Position getPos() {
        return pos;
    }
}
