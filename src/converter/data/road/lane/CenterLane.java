package converter.data.road.lane;

public class CenterLane extends Lane {

    public CenterLane(double roadMarkOffset, RoadMarkType roadMarkType, double roadMarkWidth) {
        super(0, roadMarkOffset, roadMarkType, roadMarkWidth);
    }
}
