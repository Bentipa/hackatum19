package converter.data.road;

public class RoadSignal {

    private double s;
    private double t;

    public RoadSignal(double s, double t) {
        this.s = s;
        this.t = t;
    }

    public double getS() {
        return s;
    }

    public double getT() {
        return t;
    }

}
