package converter.data;

public enum RoadType {
    ROAD, JUNCTION;
}
