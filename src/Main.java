import converter.data.OpenDriveDataContainer;
import converter.exporter.ExportData;
import converter.exporter.file.FileExporter;
import converter.exporter.file.obj.ObjExporter;
import converter.exporter.mesh.MeshExport;
import converter.exporter.mesh.MeshExporter;
import converter.importer.FileInputReader;
import converter.importer.XMLParser;

public class Main {

    public final static String IMPORT_PATH = "samples\\Crossing8Course.xodr";
    public final static String EXPORT_PATH = "C:\\Users\\Flori\\Desktop\\HackaTUM\\Export\\Test1";

    public static void main(String[] args) {

        String input = FileInputReader.readFromFile(IMPORT_PATH);
        if(input != null){

            OpenDriveDataContainer con = XMLParser.parse(input);
            if(con != null){

                MeshExport meshExport = MeshExporter.export(con);
                ExportData exportData = ObjExporter.export(EXPORT_PATH, meshExport);

                FileExporter.exportToFiles(exportData);

            }

        }

    }

}
